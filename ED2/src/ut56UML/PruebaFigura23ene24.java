/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ut56UML;


/**
 *
 * @author Luis Navarro
 */
public class PruebaFigura23ene24 {
    public static void main(String[] args) {
        Rectangulo r1=new Rectangulo(2, 2, "rojo", 2, 3);
        Circulo c1=new Circulo(5,5,"verde",5);
        Cuadrado d1=new Cuadrado(8,8, "azul", 7);
        Figura[] misFigura=new Figura[3];
        misFigura[0]=r1;
        misFigura[1]=c1;
        misFigura[2]=d1;
        for (int i = 0; i < misFigura.length; i++) {
            System.out.println("El area de"+ misFigura[i] +" es " +misFigura[i].area());
        }
        
    }
}
