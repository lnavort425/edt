//package geometria;
package ut56UML;

public abstract class Figura {

    protected Punto centro;
    private String color;

    public Figura(double x, double y, String F) {
        centro = new Punto(x, y);
        this.color = F;
    }

    public double getXCentro() {
        return centro.getX();
    }

    public double getYCentro() {
        return centro.getY();
    }

    public String getColor() {
        return color;
    }

    public void setXCentro(double x) {
        centro.setX(x);
    }

    public void setYCentro(double y) {
        centro.setY(y);
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public String toString(){
        return "Soy figura de color "+color+" y centro "+centro;
    }

    public abstract double perimetro();

    public abstract double area();

}
