/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edt2;

import java.util.Scanner;

/**
 *
 * @author luisnavarro Clase para ilustrar errores de ejecución, compilación,
 * linkado....
 */
public class EcuacionSegundoGrado {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int a, b, c;
        int d = 0;
        System.out.println("Dime los coeficientes de una raiz de segundo grado, a,b, c tal que ax^2+bx+c=0 :");
        a = teclado.nextInt();
        b = teclado.nextInt();
        c = teclado.nextInt();
        System.out.println("La ecuación a resolver es: " + a + "x^2+" + b + "x+" + c + "=0");
        //Veo el valor del determinante.... b*b-4*a*c
        d = b * b - 4 * a * c;
        if (d < 0) {
            System.out.println("La ecuación no tiene solución");
        } else if (d == 0) {
            System.out.println("La ecuación tiene una única solución doble que es: " + (-b / 2 * a));
        } else //d>0
        {
            System.out.println("La ecuación tiene dos soluciones:");
            System.out.println(-b - Math.sqrt(d) / 2 * a);
            System.out.println(-b + Math.sqrt(d) / 2 * a);
        }

    }
    public static String raices(int a, int b, int c) {
        System.out.println("La ecuación a resolver es: " + a + "x^2+" + b + "x+" + c + "=0");
        if ((a == 0)&&(b==0)) {
            throw new IllegalArgumentException("Argumento ilegal: las ecuaciones de segundo grado no pueden tener a=0 y b=0.");
        }
        if (a == 0) {
            throw new ArithmeticException("No es ecuación de segundo grado si a=0 y provocamos una división por cero.");
        }
        //Veo el valor del determinante.... b*b-4*a*c
        int d = b * b - 4 * a * c;
        if (d < 0) {
            return ("La ecuación no tiene solución");
        } else if (d == 0) {
            return ("La ecuación tiene una única solución doble que es: " + (-b / (2 * a)));
        } else //d>0
        {
            double s1 = (-b - Math.sqrt(d) / 2 * a);
            double s2 = (-b + Math.sqrt(d) / 2 * a);

            return ("La ecuación tiene dos soluciones:" + s1 + " y " + (-b + Math.sqrt(d) / 2 * a));
        }

    }

    public static String raices2(int a, int b, int c) {
        System.out.println("La ecuación a resolver es: " + a + "x^2+" + b + "x+" + c + "=0");
        //Veo el valor del determinante.... b*b-4*a*c
        int d = b * b - 4 * a * c;
        if (d < 0) {
            return ("La ecuación no tiene solución");
        } else if (d == 0) {
            return ("La ecuación tiene una única solución doble que es: " + (-b / (2 * a)));
        } else //d>0
        {
            double s1 = (-b - Math.sqrt(d) / 2 * a);
            double s2 = (-b + Math.sqrt(d) / 2 * a);

            return ("La ecuación tiene dos soluciones:" + s1 + " y " + (-b + Math.sqrt(d) / 2 * a));
        }

    }

    public static String raices3(int a, int b, int c) throws IllegalArgumentException,ArithmeticException{
        System.out.println("La ecuación a resolver es: " + a + "x^2+" + b + "x+" + c + "=0");
        if ((a == 0)&&(b==0)) {
            throw new IllegalArgumentException("Argumento ilegal: las ecuaciones de segundo grado no pueden tener a=0 y b=0.");
        }
        if (a == 0) {
            throw new ArithmeticException("No es ecuación de segundo grado si a=0 y provocamos una división por cero.");
        }
        //Veo el valor del determinante.... b*b-4*a*c
        int d = b * b - 4 * a * c;
        double rd = Math.sqrt(d);
        double e=d/a;
        double s1 = (-b - Math.sqrt(d) / (2 * a));
        double s2 = (-b + Math.sqrt(d) / (2 * a));

        return ("La ecuación tiene dos soluciones:" + s1 + " y " + (-b + Math.sqrt(d) / 2 * a));

    }
    
}
