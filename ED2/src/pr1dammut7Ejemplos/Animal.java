/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1dammut7Ejemplos;

/**
 *
 * @author Luis Navarro
 */
//"Una clase Animal la cual tiene un metodo ""hacerRuido"" y varias subclases con animales como perro, etc. Al ser subclases de animal heredan el metodo ""hacerRuido"". Cada subclase dentro tiene otro metodo que tambien sea ""hacerRuido"" el cual sobreescribe el que han heredado.
public class Animal {
    void hacerSonido() {
        System.out.println("*Ruido de animales*");
   }
    public static void main(String[] args) {
        Animal perro = new Perro();
        Animal gato = new Gato();
        Animal[] a=new Animal[2];
        a[0]=perro;
        a[1]=gato;
        for (int i = 0; i < 2; i++) {
            a[i].hacerSonido();
        }
    }
}
class Perro extends Animal {
    @Override
    void hacerSonido() {
        System.out.println("Woof");
    }
}
class Gato extends Animal {
    @Override
    void hacerSonido() {
        System.out.println("miauuu");
    }
}