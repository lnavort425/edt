/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1dammut7Ejemplos;

import java.time.LocalDate;

/**
 *
 * @author Luis Navarro
 */
/**
 * Clase Profesor
 * Clase que contiene los atributos que representan a un profesor
 */
public class Profesor extends Persona {
    String especialidad;
    double salario; 

    // Constructor
    // -----------
    
    /**
     * Constructor de la clase Persona
     * @param nombre          Nombre del profesor
     * @param apellidos       Apellidos del profesor
     * @param fechaNacimiento Fecha de nacimiento del profesor
     * @param especialidad    Especialidad del profesor
     * @param salario         Salario del profesor
     */
    public Profesor (String nombre, String apellidos, 
            LocalDate fechaNacimiento, String especialidad, 
            double salario) {
        super (nombre, apellidos, fechaNacimiento);
        this.especialidad= especialidad;
        this.salario= salario;            
    }


    /** 
     * Getter de la especialidad del profesor
     * @return Especialidad del profesor
     */
    public String getEspecialidad (){
        return especialidad;
    }

    /**
     * Getter del salario del profesor
     * @return Salario del profesor
     */
    public double getSalario (){
        return salario;
    }

    /**
     * Setter del salario del profesor
     * @param salario Salario del profesor
     */
    public void setSalario (double salario){
        this.salario= salario;
    }

    /**
     * Setter de la especialidad del profesor
     * @param especialidad Especialidad del profesor
     */
    public void setESpecialidad (String especialidad){
        this.especialidad= especialidad;
    }
            public void saluda()
    {
        System.out.println("hola, soy "+nombre+" y nací "+fechaNacimiento+" y mi especialidad es "+especialidad+" y mi sueldo:"+salario);
    }
}